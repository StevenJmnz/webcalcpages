const thcValues = {
  joint: { small: 10, medium: 20, large: 30 },
  brownie: { small: 15, medium: 25, large: 40 },
  pasta: { small: 20, medium: 35, large: 50 },
  pipe: { small: 10, medium: 15, large: 25 },
  bong: { small: 15, medium: 25, large: 35 },
  pen: { small: 5, medium: 10, large: 15 },
  cookie: { small: 15, medium: 25, large: 35 },
  gummy: { small: 5, medium: 10, large: 15 },
  tea: { small: 5, medium: 10, large: 15 },
  dab: { small: 30, medium: 50, large: 70 },
  tincture: { small: 5, medium: 15, large: 25 },
};

const effectsDescriptions = {
  low: "Efecto leve: Relajación y alivio del estrés, aún consciente y alerta.",
  medium: "Efecto moderado: Sentimiento de euforia, ligera pérdida de coordinación y mayor somnolencia.",
  high: "Efecto intenso: Posible pérdida de sobriedad y coordinación, efectos sedativos marcados.",
};

const durationMap = {
  low: { multiplier: 1.5 },       // Duración estimada para baja tolerancia
  medium: { multiplier: 1.0 },    // Duración estimada para tolerancia media
  high: { multiplier: 0.5 },      // Duración estimada para alta tolerancia
};

let timerInterval;

function calculateTHC() {
  const objectType = document.getElementById("objectType").value;
  const size = document.getElementById("size").value;
  const toleranceLevel = document.getElementById("toleranceLevel").value;

  const thcContent = thcValues[objectType][size] || 0;
  document.getElementById("result").innerText = `El contenido de THC es: ${thcContent} mg`;

  updateScale(thcContent, toleranceLevel);
  addToHistory(objectType, size, thcContent);
  startEffectTimer(thcContent, toleranceLevel);
}

function updateScale(thcContent, toleranceLevel) {
  const plantContainer = document.querySelector(".plant-icons");
  let scaleLevel = Math.min(Math.floor(thcContent / 5), 10);

  if (toleranceLevel === "medium") scaleLevel *= 0.8;
  if (toleranceLevel === "high") scaleLevel *= 0.5;

  plantContainer.innerHTML = "";
  for (let i = 0; i < scaleLevel; i++) {
    const plant = document.createElement("span");
    plant.classList.add("plant", "green");
    plant.innerText = "🌿";
    plantContainer.appendChild(plant);
  }

  const sobrietyIndicator = document.getElementById("sobrietyIndicator");
  sobrietyIndicator.classList.remove("low", "medium", "high");

  let effectLevel;
  if (scaleLevel <= 3) {
    sobrietyIndicator.classList.add("low");
    effectLevel = "low";
  } else if (scaleLevel <= 7) {
    sobrietyIndicator.classList.add("medium");
    effectLevel = "medium";
  } else {
    sobrietyIndicator.classList.add("high");
    effectLevel = "high";
  }

  document.getElementById("effectsDescription").innerText = effectsDescriptions[effectLevel];
}

function addToHistory(objectType, size, thcContent) {
  const historyList = document.getElementById("consumptionHistory");
  const listItem = document.createElement("li");
  listItem.textContent = `${objectType} (${size}) - ${thcContent} mg de THC`;
  historyList.prepend(listItem);
}

function startEffectTimer(thcContent, toleranceLevel) {
  clearInterval(timerInterval);

  // Calcula duración en base al THC y nivel de tolerancia, en horas
  const durationHours = Math.ceil((thcContent / 10) * durationMap[toleranceLevel].multiplier);
  let timeRemaining = durationHours * 60 * 60; // convierte horas a segundos

  document.getElementById("timerContainer").style.display = "block";

  function updateTimer() {
    const hours = Math.floor(timeRemaining / 3600);
    const minutes = Math.floor((timeRemaining % 3600) / 60);
    const seconds = timeRemaining % 60;
    document.getElementById("timerDisplay").textContent = `${hours}:${minutes < 10 ? "0" : ""}${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;

    if (timeRemaining <= 0) {
      clearInterval(timerInterval);
      document.getElementById("timerDisplay").textContent = "Efecto finalizado";
    }
    timeRemaining--;
  }

  updateTimer();
  timerInterval = setInterval(updateTimer, 1000);
}

function resetTimer() {
  clearInterval(timerInterval);
  document.getElementById("timerDisplay").textContent = "0:00";
  document.getElementById("timerContainer").style.display = "none";
}
